import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortByName',
  pure: false
})
export class SortByNamePipe implements PipeTransform {

  transform(value: any, field: string = 'name'): any {
    if (!value) {
      return value;
    }
    return value.sort((a, b) => a[field].localeCompare(b[field]));
  }

}
