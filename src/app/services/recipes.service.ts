import { Injectable } from '@angular/core';

import { Recipe } from '../models/recipe.model';
import { Ingredient } from '../models/ingredient.model';
import { ShoppingListService } from './shopping-list.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {
  recipesChanged = new Subject<Recipe[]>();

  _recipes: Recipe[] = [
    new Recipe(
      1,
      'Pizza 4 seasons',
      'Pizza (Italian: [ˈpittsa], Neapolitan: [ˈpittsə]) is a savory dish of Italian origin.',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYiWgRscPWCUBdue_T93lCeh6p7nsQPo0fLiJkslCHEK4Yzi8O&s',
      [
        new Ingredient('egg', 5),
        new Ingredient('butter', 2),
        new Ingredient('bread', 3)
      ]
    ),
    new Recipe(
      2,
      'Big fork burger',
      'A hamburger (short: burger) is a sandwich consisting of one or more cooked patties of ground meat..',
      'https://caughtinsouthie.com/wp-content/uploads/TASTY-BURGER%20IMAGE%20FROM%20AD.jpg',
      [
        new Ingredient('salad', 7),
        new Ingredient('oil', 2),
        new Ingredient('tomato', 3)
      ]
    )
  ];

  constructor(private shoppingListService: ShoppingListService) {
  }

  get recipes() {
    return [...this._recipes];
  }

  getRecipeById(id: number) {
    return this.recipes.find(recipe => recipe.id === id);
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.shoppingListService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe) {
    this._recipes.push(recipe);
    this.recipesChanged.next(this.recipes);
  }

  updateRecipe(recipe: Recipe, id: number) {
    const updateRecipeIndex = this.recipes.findIndex(r => r.id === id);
    this._recipes[updateRecipeIndex] = recipe;
    this.recipesChanged.next(this.recipes);
  }

  deleteRecipe(id: number) {
    this._recipes = this._recipes.filter(recipe => recipe.id !== id);
    this.recipesChanged.next(this.recipes);
  }
}
